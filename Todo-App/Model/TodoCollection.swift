//
//  TodoCollection.swift
//  Todo-App
//
//  Created by 大平譲 on 2019/01/31.
//  Copyright © 2019 大平譲. All rights reserved.
//

import UIKit

class TodoCollection: NSObject {
//    Todoの集合を管理するための配列
    var todos: [Todo] = []
//        シングルトンな変数を定義
    static let sheredInstance = TodoCollection()
    
//    Todoを配列に追加するメソッド
    func addTodoCollection(todo: Todo) {
        self.todos.append(todo)
        self.save()
    }
    
    func save() {
        var todoList: Array<Dictionary<String, AnyObject>> = []
        for todo in todos {
            let todoDic = TodoCollection.convertDictionary(todo: todo)
            todoList.append(todoDic)
        }
//        UserDefaultsクラスとはアプリを停止させてもずっと保持しておきたい情報を保持する為のクラス
        let defaults = UserDefaults.standard
        defaults.set(todoList, forKey: "todoLists")
//        synchronize()メソッドはメモリ上に一時保存されたデータをストレージに保存するメソッドです。
        defaults.synchronize()
    }
//    Todoクラスのインスタンスを変換するものなので、TodoCollectinクラス内で定義するにはクラスメソッドとして定義する必要があります。よってTodoクラスのインスタンスは引数として渡しましょう。また戻り値として、辞書型を返すようにし、キーはString型、値のデータ型はAnyObject型として記述します。
    class func convertDictionary(todo: Todo) -> Dictionary<String, AnyObject> {
        var dictionary = Dictionary<String, AnyObject>()
        dictionary["title"] = todo.title as AnyObject
        dictionary["descript"] = todo.descript as AnyObject
        dictionary["priority"] = todo.priority.rawValue as AnyObject
        return dictionary
    }
    
    func fetchTodo() {
        let defaults = UserDefaults.standard
//        辞書型に変換するのでダウンキャスト
//        as?演算子はダウンキャストする事が出来ない場合は値としてnilを返す
        if let todoList = defaults.object(forKey: "todoLists") as? Array<Dictionary<String, AnyObject>> {
            for todoDic in todoList {
                let todo = TodoCollection.convertTodoModel(attributes: todoDic)
                self.todos.append(todo)
            }
        }
    }
//    保存した辞書型のTodoのデータをTodo型に変換していくクラスメソッド
    class func convertTodoModel(attributes: Dictionary<String, AnyObject> ) -> Todo {
        let todo = Todo()
        todo.title = attributes["title"] as! String
        todo.descript = attributes["descript"] as! String
        todo.priority = TodoPriority(rawValue: attributes["priority"] as! Int)!
        return todo
    }
}
