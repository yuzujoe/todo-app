//
//  Todo.swift
//  Todo-App
//
//  Created by 大平譲 on 2019/01/31.
//  Copyright © 2019 大平譲. All rights reserved.
//

import UIKit

class Todo: NSObject {
    var title = ""
    var descript = ""
//    enumであらかじめ定義した型名で型指定を行うと、呼び出すときは.メンバ名とすれば呼び出せるようになる
    var priority:TodoPriority = .Low
    
}

enum TodoPriority:Int {
    case Low = 0
    case Middle = 1
    case High = 2
    
    func color() -> UIColor {
        switch self {
        case .Low:
            return UIColor.yellow
        case .Middle:
            return UIColor.green
        case .High:
            return UIColor.red
        }
    }
}
