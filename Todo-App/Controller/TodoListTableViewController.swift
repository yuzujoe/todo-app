//
//  TodoListTableViewController.swift
//  Todo-App
//
//  Created by 大平譲 on 2019/01/31.
//  Copyright © 2019 大平譲. All rights reserved.
//

import UIKit

class TodoListTableViewController: UITableViewController {
//    TodoCollectionクラスのインスタンスを宣言
    let todoCollection = TodoCollection.sheredInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        todoCollection.fetchTodo()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        tableViewの数が投稿したものと同じになるようにcountする
        return self.todoCollection.todos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "reuseIdentifier")
//        配列からTodoを取得する
        let todo = self.todoCollection.todos[indexPath.row]
//        titleプロパティを使ってcellのタイトルを設定
        cell.textLabel!.text = todo.title
        cell.detailTextLabel!.text = todo.descript
        cell.textLabel!.font = UIFont(name: "HirakakuProN-W3", size: 15)
        let priorityIcon = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        priorityIcon.layer.cornerRadius = 6
        priorityIcon.backgroundColor = todo.priority.color()
//      accessoryView、セルの右側にアイコンなどを表示
        cell.accessoryView = priorityIcon
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            self.todoCollection.todos.remove(at: indexPath.row)
            self.todoCollection.save()
//            deleteRows(at:with:)メソッド:テーブルのセルを削除していく際に、データが0件の場合には新たに一つ何も無いセルを表示し、データが1件以上ある場合はデータの数だけセルを表示させるメソッド
//            引数atには削除する行を識別するオブジェクトの配列を
//            引数withにはセルが消えるときのアニメーションを設定
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.middle)
        default:
            return
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt souceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        選択したセルのTodoのインスタンスを取り出す
        let todo = self.todoCollection.todos[souceIndexPath.row]
//         選択したTodoのインスタンスを配列から削除する
        self.todoCollection.todos.remove(at: souceIndexPath.row)
//        取り出したインスタンスを配列の移動したセルのRow数番目に挿入する
//        insertメソッドは配列に対して使い、入れたい要素と入れたい場所が何番目かを指定する事によって、配列に指定した要素と指定した順番の場所に挿入する
        self.todoCollection.todos.insert(todo, at: destinationIndexPath.row)
//        順番を入れ替えた配列を保存する(saveメソッドを使う)
        self.todoCollection.save()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新規作成", style: UIBarButtonItem.Style.plain, target: self, action:  #selector(TodoListTableViewController.newTodo))
        self.navigationItem.leftBarButtonItem = editButtonItem
        self.tableView.reloadData()
    }
    
    @objc func newTodo() {
        self.performSegue(withIdentifier: "PresentNewTodoViewController", sender: self)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
