//
//  NewTodoViewController.swift
//  Todo-App
//
//  Created by 大平譲 on 2019/01/31.
//  Copyright © 2019 大平譲. All rights reserved.
//

import UIKit

class NewTodoViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var todoField: UITextField!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var prioritySegment: UISegmentedControl!
    let todoCollection = TodoCollection.sheredInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionView.layer.cornerRadius = 5
        descriptionView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionView.layer.borderWidth = 1
        
//        タップを認識するためのクラス
        let tapRecofnizer = UITapGestureRecognizer(target: self, action: #selector(NewTodoViewController.tapGesture(_:)))
        self.view.addGestureRecognizer(tapRecofnizer)
        todoField.delegate = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController!.navigationBar.tintColor = UIColor.white
//        閉じるボタンの作成を左側に
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "閉じる", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NewTodoViewController.close))
//        保存ボタンの作成を右側に
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "保存", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NewTodoViewController.save))
    }

    @objc func close() {
//        dismissでモーダルのviewを閉じるためのメソッド
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func save() {
        if todoField.text!.isEmpty {
            let aleatView = UIAlertController(title: "エラー", message: "必要な項目が記入されていません", preferredStyle: UIAlertController.Style.alert)
            aleatView.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(aleatView, animated: true, completion: nil)
            
        } else {
            let todo = Todo()
            todo.title = todoField.text!
            todo.descript = descriptionView.text
            todo.priority = TodoPriority(rawValue: prioritySegment.selectedSegmentIndex)!
            self.todoCollection.addTodoCollection(todo: todo)
            print(self.todoCollection.todos)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
//    タップしたらキーボードが消える処理
    @objc func tapGesture(_ sender: UITapGestureRecognizer) {
//        キーボードを閉じるという処理
        todoField.resignFirstResponder()
        descriptionView.resignFirstResponder()
    }
    
//    returnキーを押すとキーボードが消える処理
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        todoField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
